<?php
namespace Drupal\urls_alias_manager;
/*
*
*Description: config class for set, get, and delete all config by name, and key.
*/
class ConfigAPI{
    //============================== Refactored ==========================
    private static function init($configName){
        $config = \Drupal::service('config.factory')->getEditable($configName);
        return $config;
    }
    public static function get($configName, $key){
        $config = ConfigAPI::init($configName);
        return $config->get(strtolower($key));
    }
    public static function set($configName, $key, $value){
        $config = ConfigAPI::init($configName);
        return $config->set(strtolower($key), $value)->save();
    }
    public static function deleteAll($configName){
        \Drupal::service('config.factory')->getEditable($configName)->delete();
    }
}