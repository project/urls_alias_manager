<?php
namespace Drupal\urls_alias_manager;
use Drupal\urls_alias_manager\ConfigAPI;
/*
*
*Description: Main class that handle the URL Alias validation
*/

class UrlAliasManager{
    protected function getEditableConfigNames() {
        return 'urls_alias_manager.url_alias_manager';
    }
    /*
    *
    *Description: Class method to convert the Number to word for the validation error messages
    */
    private function convertNumberToWord($num = false){
        $num = str_replace(array(',', ' '), '' , trim($num));
        if(! $num) {
            return false;
        }
        $num = (int) $num;
        $words = array();
        $list1 = array('', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven',
            'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'
        );
        $list2 = array('', 'ten', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety', 'hundred');
        $list3 = array('', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion',
            'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quattuordecillion',
            'quindecillion', 'sexdecillion', 'septendecillion', 'octodecillion', 'novemdecillion', 'vigintillion'
        );
        $num_length = strlen($num);
        $levels = (int) (($num_length + 2) / 3);
        $max_length = $levels * 3;
        $num = substr('00' . $num, -$max_length);
        $num_levels = str_split($num, 3);
        for ($i = 0; $i < count($num_levels); $i++) {
            $levels--;
            $hundreds = (int) ($num_levels[$i] / 100);
            $hundreds = ($hundreds ? ' ' . $list1[$hundreds] . ' hundred' . ' ' : '');
            $tens = (int) ($num_levels[$i] % 100);
            $singles = '';
            if ( $tens < 20 ) {
                $tens = ($tens ? ' ' . $list1[$tens] . ' ' : '' );
            } else {
                $tens = (int)($tens / 10);
                $tens = ' ' . $list2[$tens] . ' ';
                $singles = (int) ($num_levels[$i] % 10);
                $singles = ' ' . $list1[$singles] . ' ';
            }
            $words[] = $hundreds . $tens . $singles . ( ( $levels && ( int ) ( $num_levels[$i] ) ) ? ' ' . $list3[$levels] . ' ' : '' );
        } //end for loop
        $commas = count($words);
        if ($commas > 1) {
            $commas = $commas - 1;
        }
        return implode(' ', $words);
    }
    /*
    *
    *Description: main class static function
    */
    public static function handleAliasValidation(&$form_state){
        $pathAuto = $form_state->getValue('path')[0]["pathauto"];
        if($pathAuto == 1 ){
            return;
        }
        $urlsConfigurations = ConfigAPI::get(self::getEditableConfigNames(), "UrlAliasManagerConfigurations");
        $configArray = $urlsConfigurations;
        $node = $form_state->getFormObject()->getEntity();
        $nodeType = $node->getType();
        if (isset($configArray[$nodeType]) == false){
            return true;
        }
        $alias = $form_state->getValue('path')[0]["alias"];  
        if ($alias == "" && $configArray[$nodeType]["allow_empty"] == 0){
            $form_state->setErrorByName('url_alias_validation', t('URL alias shouldn\'t be empty'));
        }

        if ($node->isNew() || $configArray[$nodeType]["validate_existed"] == 1) {
            if (self::validateUrlAlias($alias, $nodeType, $configArray) == false){
                $parts = $configArray[$nodeType]["url_parts"];
                $partsWord = self::convertNumberToWord($parts);
                $plurar = "";
                if ($parts >1 ){
                    $plurar = "s";
                }
                $form_state->setErrorByName('url_alias_validation', t('URL alias shouldn\'t be longer than'.$partsWord.' part'.$plurar.' for example: /path_1/path_2/page.html'));
            }
        }
      }
    /*
    *
    *Description: Class method count the parts and compare it to the stored config and return True and False
    */
    private function validateUrlAlias($alias,$nodeType, $configArray){
        $limit = intval($configArray[$nodeType]["url_parts"]);
        $limit = $limit + 1;
        $parts  =  explode("/",$alias);
        if (count($parts)>$limit){
            return false;
        }else{
            return true;
        }
    }
}