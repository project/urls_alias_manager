<?php
namespace Drupal\urls_alias_manager\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\urls_alias_manager\ConfigAPI;
class UrlAliasManagerForm extends ConfigFormBase {
  protected function getEditableConfigNames() {
    return 'urls_alias_manager.url_alias_manager';
  }
  public function buildForm(array $form, FormStateInterface $form_state) {
    $selectOptions = UrlAliasManagerForm::getTypes();
    $urlsConfigurations = ConfigAPI::get($this->getEditableConfigNames(), "UrlAliasManagerConfigurations");


     $form['urls_manager'] = array(
      '#type' => 'details',
      '#title' => $this->t('URLS Manager'),
      '#open' => TRUE,
    );

    $form['urls_manager']['description']  = array(
      '#markup' => '<p>Note: Pathauto will override this feature</p>',
    );
    //echo print_r(ConfigAPI::get($this->getEditableConfigNames(), "UrlAliasManagerMainSwitch")." dslkjflksdj");
    $form['urls_manager']['enable_urls_manager'] = array(
      '#type' => 'checkbox',
      '#title' => t('Enable'),
      '#default_value'=>ConfigAPI::get($this->getEditableConfigNames(), "UrlAliasManagerMainSwitch")
    );
    $form['description'] = array(
      '#markup' => '<div>' . t('') . '</div>',
    );
    $i = 0;
    $form['#tree'] = TRUE;
    $name_field = count($urlsConfigurations);//$form_state->get('num_names');
    if ($name_field == 0) {
      $urlsConfigurations = array();
      $urlsConfigurations["false"] = array("allow_empty"=>0, "url_parts"=>1, "validate_existed"=>0);
    }
    $form['import_items'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Content Types URL Alias Setting'),
      '#prefix' => '<div id="names-fieldset-wrapper">',
      '#suffix' => '</div>',
    ];
    foreach ($urlsConfigurations as $contentType => $properties) {  
      $form['import_items'][$i]['content_type'] = array(
        '#type' => 'select',
        '#title' => t('Content Type'),
        '#description'=>t(''),
        '#options' =>$selectOptions,
        '#prefix' => '<div class="manual-import-item-field manual-import-item-device>',
        '#suffix' => '</div>',
        '#default_value'=>$contentType
      );
      $values = $form_state->getValue(array('import_items'));
      $contentType2 = $values[$i]["content_type"];
      unset($selectOptions[$contentType2]);
      
      $form['import_items'][$i]['allow_empty'] = array(
        '#type' => 'checkbox',
        '#title' => t('Allow Empty URL Alias?'),
        '#default_value'=>$properties["allow_empty"],
        '#prefix' => '<div class="manual-import-item-field manual-import-item-device-id">',
        '#suffix' => '</div>',
      );

      $form['import_items'][$i]['validate_existed'] = array(
        '#type' => 'checkbox',
        '#title' => t('Validate Existed Data?'),
        '#prefix' => '<div class="manual-import-item-field manual-import-item-device-id">',
        '#suffix' => '</div>',
        '#default_value'=>$properties["validate_existed"],
      );

      $form['import_items'][$i]['url_parts'] = array(
        '#type' => 'number',
        //'#min'=>1,
        '#title' => t('How Many Maximum Parts?'),
        '#description'=>t('for example: /hr/officeOne/index.html this has three parts'),
        '#prefix' => '<div class="manual-import-item-field manual-import-item-device-id">',
        '#suffix' => '</div>',
        '#default_value'=>$properties["url_parts"]
      );
      $form['import_items'][$i]['hr']  = array(
        '#markup' => '<hr><br>',
      );
      $i++;
    }
    $form['actions'] = [
      '#type' => 'actions',
    ];
    
    $form['import_items']['actions']['add_name'] = [
      '#type' => 'submit',
      '#value' => t('Add one more'),
      '#submit' => array('::addOne'),
      '#ajax' => [
        'callback' => '::addmoreCallback',
        'wrapper' => 'names-fieldset-wrapper',
      ],
    ];
    
    if ($name_field > 1) {
      $form['import_items']['actions']['remove_name'] = [
        '#type' => 'submit',
        '#value' => t('Remove one'),
        '#submit' => array('::removeCallback'),
        '#ajax' => [
          'callback' => '::addmoreCallback',
          'wrapper' => 'names-fieldset-wrapper',
        ],
      ];
    }
    $form_state->setCached(FALSE);
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];

    return $form;
  }

  public function getFormId() {
    return 'fapi_example_ajax_addmore';
  }

  public function addmoreCallback(array &$form, FormStateInterface $form_state) {
    $name_field = $form_state->get('num_names');
    return $form['import_items'];
  }

  public function addOne(array &$form, FormStateInterface $form_state) {
    $emptyItem = array("allow_empty"=>0, "url_parts"=>1, "validate_existed"=>0);
    $current=ConfigAPI::get($this->getEditableConfigNames(), "UrlAliasManagerConfigurations");
    $numberOfItems = count($current);
    $numberOfItems++;
    $current["false_".$numberOfItems] = $emptyItem;
    ConfigAPI::set($this->getEditableConfigNames(), "UrlAliasManagerConfigurations",$current);
    $form_state->setRebuild();
  }

  public function removeCallback(array &$form, FormStateInterface $form_state) {
    $current=ConfigAPI::get($this->getEditableConfigNames(), "UrlAliasManagerConfigurations");
    array_pop($current); 
    ConfigAPI::set($this->getEditableConfigNames(), "UrlAliasManagerConfigurations",$current);
    $form_state->setRebuild();
  }

  public function submitForm(array &$form, FormStateInterface $form_state) {
    ConfigAPI::deleteAll($this->getEditableConfigNames());
    $values = $form_state->getValue(array('import_items'));
    drupal_set_message("configuration saved successfully");
   
   $configArray=array();
    foreach ($values as $key => $value) {
      $contentType = $value["content_type"];
      if ($contentType != "false_" && $contentType != "" ){
        $allow_empty = $value["allow_empty"];
      
        $url_parts = $value["url_parts"];
        $validate_existed = $value["validate_existed"];
        $configArray[$contentType]= array("allow_empty"=>$allow_empty, "url_parts"=>$url_parts, "validate_existed"=>$validate_existed);
      }
    }
    ConfigAPI::set($this->getEditableConfigNames(), "UrlAliasManagerConfigurations",$configArray);
    $manager_enable = $form_state->getValue("urls_manager")["enable_urls_manager"];
    ConfigAPI::set($this->getEditableConfigNames(), "UrlAliasManagerMainSwitch",$manager_enable);
  }
  public function getTypes(){
    $types = \Drupal::entityTypeManager()->getStorage('node_type')->loadMultiple();
    $selectOptions=array("false_"=>"--");
    foreach ($types as $typeMachineName => $typeProperties) {
    $selectOptions[$typeMachineName]=$typeProperties->get("name");
    }
    return $selectOptions;
  }
}